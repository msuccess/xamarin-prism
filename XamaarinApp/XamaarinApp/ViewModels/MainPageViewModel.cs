﻿using Prism.Commands;
using Prism.Navigation;

namespace XamaarinApp.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public DelegateCommand Page1Command { get; set; }
        public DelegateCommand Page2Command { get; set; }

        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Main Page";

            Page1Command = new DelegateCommand(ExecuteCommandPageOne);

            Page2Command = new DelegateCommand(ExecuteCommandPageTwo);
        }

        public async void ExecuteCommandPageOne()
        {
            await NavigationService.NavigateAsync("PrismPage1");
        }

        public async void ExecuteCommandPageTwo()
        {
            await NavigationService.NavigateAsync("PrismPage2");
        }
    }
}