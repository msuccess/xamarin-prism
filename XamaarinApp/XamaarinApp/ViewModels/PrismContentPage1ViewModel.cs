﻿using Prism.Navigation;

namespace XamaarinApp.ViewModels
{
    public class PrismContentPage1ViewModel : ViewModelBase
    {
        public PrismContentPage1ViewModel(INavigationService navigationService) :
            base(navigationService)
        {
        }
    }
}